# Notes

## Components

### App

Description:

The main component

### Header

Description:

The header component of the page

- having oorjan logo on the left
- search icon button on the right (v2)

### Search

Description:

The main search component containing inputs, filters

Layout:

- search input
  - search button will be next to it
  - show random placeholders (optional)
- filters
  - position: below search input
  - visibility: hidden by default
  - order: asc,desc; orderby: date,relevance;

### List

Description:

By default show 5 latest blog posts
Search component's response will render the List component.

### Card

Description:

The blog card
Rendered by List component

Layout:

- Cover image on the right
- On the left:
  - Title
  - Excerpt
  - Category, tags, author name, published date

## Methods

### fetchPosts

```js
  /*
    query: string The search query if any
    filters: object
    {
      order: (asc|desc*),
      orderby: (date|relevance*)
      limit: 5*
    }
  */
  fetchPosts(query, filters)
```

## TODOS

### Done

- Style the header
- Style the search input
- Style the cards
- fix titles, excerpts
- Disable relevance selector when query is empty
- add icon in search
- add a clear input button which will reset the field
- add empty msg
- add loader while fetching / add shimmer
- fix responsiveness
- cleanup

### In Progress

### Backlog
