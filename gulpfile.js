const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("autoprefixer");
const sourcemaps = require("gulp-sourcemaps");
const browserSync = require("browser-sync").create();
const useref = require("gulp-useref");
const gulpIf = require("gulp-if");
const cssnano = require("gulp-cssnano");
const imagemin = require("gulp-imagemin");
const cache = require("gulp-cache");
const postcss = require("gulp-postcss");
const del = require("del");
const runSequence = require("run-sequence");
const babel = require("gulp-babel");
const sitemap = require("gulp-sitemap");
const notify = require("gulp-notify");
const removeLogging = require("gulp-remove-logging");
const babelify = require("babelify");
const browserify = require("browserify");
const buffer = require("vinyl-buffer");
const source = require("vinyl-source-stream");
const es = require("event-stream");
const rename = require("gulp-rename");
const glob = require("glob");
const connect = require("gulp-connect-php");
const htmlmin = require("gulp-htmlmin");
const fs = require("fs");
const plumber = require("gulp-plumber");
const argv = require("minimist")(process.argv.slice(2));

/**
 * References:
 *  1. https://stackoverflow.com/questions/23023650/is-it-possible-to-pass-a-flag-to-gulp-to-have-it-run-tasks-in-different-ways
 */

var CONFIG = require("./_config_.js");
var DIRS = CONFIG.dirs;
var SERVER = CONFIG.server;

var onError = function(err) {
	notify.onError({
		title: "Error",
		message: "<%= error %>"
	})(err);
	this.emit("end");
};

var plumberOptions = {
	errorHandler: onError
};

// --- tasks ---

gulp.task("update-config", function() {
	/**
	 * @ref
	 *  https://stackoverflow.com/questions/36856232/write-add-data-in-json-file-using-node-js
	 */
	var JSONconfig = JSON.stringify(CONFIG, null, "\t");

	fs.writeFile("_config_.json", JSONconfig, "utf8", function(e) {
		// console.log(e);
		console.info("JSON config updated!");
	});
});

// Development Tasks
// -----------------

// Start BrowserSync server
gulp.task("browserSync", function() {
	if (["info", "debug", "warn", "silent"].indexOf(argv["l"]) === -1) {
		argv["l"] = "info";
	}

	var bsConf = {
		server: {
		  baseDir: DIRS.src.root,
		  index: "index.html"
		},
		open: argv["o"] || false,
		reloadOnRestart: true,
		// host: "dev.kratee.com",
		port: SERVER.port,
		ghostMode: argv["g"] || false, // for disabling synced control
		logLevel: argv["l"], // "info", "debug", "warn", or "silent"
		logPrefix: CONFIG.name,
		logConnections: argv["l"] === "debug" || false,
		logFileChanges: argv["l"] === "debug" || false,
		logSnippet: argv["l"] === "debug" || false,
		scrollThrottle: 100,
		// proxy: 'http://localhost',
		// middleware: function (req, res, next) {
		// 	/** First middleware handler **/
		// 	console.log('before', req.url);
		// 	req.url = '/';
		// 	console.log(req.url);
		// 	next();
		// }
	};

	browserSync.init(bsConf);
});

/**
 * https://stackoverflow.com/questions/33585617/what-does-gulps-includepaths-do
 */
gulp.task("compile-sass", function() {
	return gulp
		.src(DIRS.src.scss + "/**/*.scss") // Gets all files ending with .scss in www/scss and children dirs
		.pipe(plumber(plumberOptions))
		.pipe(sourcemaps.init())
		.pipe(
			sass({
				sourceComments: "normal",
				sourceMap: "sass",
				outputStyle: "expanded",
				includePaths: [
					"node_modules/bootstrap-sass/assets/stylesheets",
				],
				errLogToConsole: true,
				onError: console.log
			}).on("error", sass.logError)
		)
		.pipe(sourcemaps.write())
		.pipe(
			postcss([
				autoprefixer({
					browsers: ["> 1%", "last 3 versions", "IE >= 8"],
					cascade: true
				})
			])
		)
		.pipe(gulp.dest(CONFIG.dirs.src.css)) // Outputs it in the css folder
		.pipe(
			browserSync.reload({
				// Reloading with Browser Sync
				stream: true
			})
		);
	/* .pipe(notify({
    message: 'Compiled Sass',
    // message: 'Compiled Sass file: <%= file.relative %> @ <%= options.date %>',
    templateOptions: {
      date: new Date()
    }
  })); */
});


gulp.task("compile-js", function(__done) {
	glob(DIRS.src.devJs + "/**/*.js", function(err, files) {
		if (err) __done(err);

		var tasks = files.map(function(entry) {
			return browserify({ entries: [entry] })
				.transform(babelify.configure({
						// compact: false,
						presets: ["react", "es2015", "stage-0"],
						plugins: [
							"react-html-attrs", "transform-class-properties", "transform-decorators-legacy"
						],
						// debug: true // enables source maps
					}))
				.bundle()
				.pipe(plumber(plumberOptions))
				.pipe(source(entry))
				.pipe(rename(function(path) {
						// make the path same as the dest dir
						var tempPath = path.dirname.replace("/src/", "/compiled/");
						// give only the path of current file that should be followed in the dest folder
						path.dirname = tempPath.replace(DIRS.src.js, "");
					}))
				.pipe(gulp.dest(DIRS.src.js))
				.pipe(browserSync.reload({
					stream: true
				}));
		});
		es.merge(tasks).on("end", __done);
	});
});

// Watchers
gulp.task("watch", function() {
	gulp.watch(DIRS.src.scss + "/**/*.scss", ["compile-sass"]);
	gulp.watch(DIRS.src.root + "/**/*.+(html|php)", browserSync.reload);
	gulp.watch(DIRS.src.devJs + "/**/*.js", ["compile-js"], browserSync.reload);
});

// Optimization Tasks
// ------------------

// Optimizing CSS and JavaScript
gulp.task("useref", function() {
	return gulp
		.src(DIRS.src.root + "/**/*.+(html|php)")
		.pipe(useref())
		.pipe(gulpIf("*.js", babel({
			presets: ["react", "es2015", "stage-0"],
			plugins: [
				"react-html-attrs", "transform-class-properties", "transform-decorators-legacy"
			],
		})))
		.pipe(gulpIf("*.html", htmlmin({
			collapseWhitespace: true,
			minifyCSS: true,
			minifyJS: true,
			minifyURLs: true,
			removeComments: true,
			keepClosingSlash: true
		})))
		// .pipe(gulpIf("*.js", minify()))
		.pipe(gulpIf("*.css", cssnano({
			mergeRules: true
		})))
		.pipe(gulp.dest(DIRS.dest.root));
});

// Optimizing Images
gulp.task("optimize-images", function() {
	return (
		gulp
			.src(DIRS.src.images + "/**/*.+(png|jpg|jpeg|gif|svg)")
			// Caching images that ran through imagemin
			.pipe(imagemin({
				interlaced: true,
				verbose: true
			}))
			.pipe(gulp.dest(DIRS.dest.images))
	);
});

// Copying fonts
gulp.task("fonts", function() {
	return gulp.src(DIRS.src.fonts + "/**/*").pipe(gulp.dest(DIRS.dest.fonts));
});

// Cleaning
gulp.task("clean", function() {
	return del.sync(DIRS.dest.root).then(function(cb) {
		return cache.clearAll(cb);
	});
});

gulp.task("clean:dest", function() {
	return del.sync([
		DIRS.dest.root + "/**/*",
		"!" + DIRS.dest.images,
		"!" + DIRS.dest.images + "/**/*"
	]);
});

// Copy files to dist
gulp.task("copy-js-plugins", function() {
	var filesToCopy = [DIRS.src.devJs + "/plugins/external/**/*"];

	return gulp
		.src(filesToCopy, {
			base: DIRS.src.root
		})
		.pipe(
			gulpIf("*.js", rename(function(__path) {
				__path.dirname = __path.dirname.replace("src\\js\\", ""); // renaming the dirname to the desired dir
				// console.log(__path);
			}))
		)
		.pipe(gulp.dest(DIRS.src.js));
});

gulp.task("copy-files", function() {
	var filesToCopy = [
		DIRS.src.css + "/**/*",
		DIRS.src.js + "/**/*",
		DIRS.src.favicons + "/**/*"
	];

	return gulp
		.src(filesToCopy, {
			base: DIRS.src.root
		})
		.pipe(gulp.dest(DIRS.dest.root));
});

gulp.task("copy:build", function() {
	// copy plugins to compiled first
	gulp.start("copy-js-plugins");

	var filesToCopy = [
		DIRS.src.root + "/assets/**/*",
		// '!'+DIRS.src.fonts+'{,/**}', // exclude
		"!" + DIRS.src.images + "{,/**}", // exclude
		DIRS.src.root + "/css/dependencies/**/*",
		DIRS.src.devJs + "/plugins/external/**/*",
		DIRS.src.root + "/*.htaccess"
		// DIRS.src.root+'/templates/**/*',
		// DIRS.src.root+'/*.+(html|php)',
	];

	return gulp
		.src(filesToCopy, {
			base: DIRS.src.root
		})
		.pipe(gulpIf("*.htaccess", rename(function(path) {
			console.log(
				'Renaming "' + path.basename + path.extname + '" -> "' + path.extname + '"'
			);
			path.basename = ""; // renaming the dirname to the desired dir
		})))
		.pipe(gulpIf("*.js", rename(function(path) {
			path.dirname = path.dirname.replace("src", "compiled");
		})))
		.pipe(gulp.dest(DIRS.dest.root));
});

gulp.task("strip-logs", function() {
	return gulp
		.src(DIRS.src.js + "/**/*.js")
		.pipe(
			removeLogging({
				// Options (optional)
				// eg:
				// namespace: ['console', 'window.console']
			})
		)
		.pipe(gulp.dest("build/javascripts/"));
});

// Sitemap
gulp.task("create-sitemap", function() {
	gulp
		.src("*.+(html|php)", {
			read: false
		})
		.pipe(
			sitemap({
				siteUrl: CONFIG.siteUrl
			})
		)
		.pipe(gulp.dest(DIRS.dest.root));
});

// Build Sequences
// ---------------

gulp.task("run-server", function(callback) {
	runSequence(
		"update-config",
		["compile-sass", "compile-js", "copy-js-plugins", "browserSync"],
		"watch",
		callback
	);
});

gulp.task("build", function(callback) {
	runSequence(
		"update-config",
		"clean:dest",
		"compile-sass",
		"compile-js",
		["useref", "optimize-images", "fonts"],
		"copy:build",
		"copy-files",
		// 'create-sitemap',
		callback
	);
});

// -------------------------------------------------------------

// References:

/**
 * compile-js
 *  http://foundation.zurb.com/forum/posts/42835-copy-some-js-files-into-dist-without-bundling-into-appjs
 *  http://macr.ae/article/gulp-and-babel.html
 *
 * copying
 *  https://stackoverflow.com/questions/21546931/use-gulp-to-select-and-move-directories-and-their-files
 *
 * glob ignoring some dirs/files
 *  https://stackoverflow.com/questions/26485612/glob-minimatch-how-to-gulp-src-everything-then-exclude-folder-but-keep-one
 *  https://stackoverflow.com/questions/23384239/excluding-files-directories-from-gulp-task
 *  https://stackoverflow.com/questions/35411576/exclude-directory-pattern-in-gulp-with-glob-in-gulp-src
 *
 * browser sync
 *  https://browsersync.io/docs/options
 *  https://browsersync.io/docs/gulp
 *  http://stackoverflow.com/questions/28962528/browsersync-cannot-get
 */
