/**
 * An extension function for getting delimiters for the current date
 */
Date.prototype.getDelimeter = function() {
  let number = this.getDate();
  var lastChar = number.toString().split('').pop(); // gives string

  if( lastChar == 1 && number != 11 ) {
    return 'st';
  } else if( lastChar == 2 ) {
    return 'nd';
  } else if (lastChar == 3) {
    return 'rd';
  }

  return 'th';
}

/**
 * An extension function for getting full month names for the current month
 */
Date.prototype.getMonthText = function() {
  const monthNames = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];

  return monthNames[this.getMonth()];
}
