import React from "react";
import Header from "./components/Header";
import Search from "./components/Search";
import List from "./components/List";
import fetchRequest from "./utils/fetchRequest";
import './extensions/Date';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      search: {
        query: '',
        lastQuery: '',
        filters: {
          order: 'desc', // asc|desc
          orderby: 'date', // date|relevance
          per_page: 5,
        },
      },
      list: [],
      loading: false,
    };

    this.handleQueryChange = this.handleQueryChange.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.parseSearchResponse = this.parseSearchResponse.bind(this);
    this.getPosts = this.getPosts.bind(this);
  }

  /**
   * Called when a query is changed
   *
   * @param {object} e
   * @param {function} callback
   */
  handleQueryChange(e, callback) {
    let searchQuery = e.target.value;

    this.setState({
      ...this.state,
      search: {
        ...this.state.search,
        query: searchQuery
      }
    }, callback);
  }

  /**
   * Called when a filter changes i.e. date,relevance or asc,desc
   *
   * @param {object} e
   */
  handleFilterChange(e) {
    let filterName = e.target.name;
    let filterValue = e.target.value;

    this.setState({
      ...this.state,
      search: {
        ...this.state.search,
        filters: {
          ...this.state.search.filters,
          [filterName]: filterValue
        }
      }
    }, () => {
      this.getPosts();
    });
  }

  /**
   * Called when a search query is made
   *
   * @param {object} e
   */
  handleSearch(e) {
    e.preventDefault();
    this.getPosts();
  }

  /**
   * Called when the user clicks on clearing the input
   */
  handleClear() {
    this.setState({
      ...this.state,
      search: {
        query: '',
        filters: {
          ...this.state.search.filters,
          orderby: 'date' // change this since relevance requires some query
        }
      }
    }, () => {
      // when the input is cleared we need to get the posts
      if (this.state.search.query !== this.state.search.lastQuery) {
        this.getPosts();
      }
    });
  }

  /**
   * Parses the raw data received from the server and
   * returns a structured object list
   *
   * @param {object} searchResponse The response received from the request
   */
  parseSearchResponse(searchResponse) {
    let data = [];
    let regex = /&#([0-9]+);|(<([^>]+)>)/ig;

    searchResponse.map((blogPost) => {
      let cardData = {};

      cardData['title'] = blogPost.title.rendered.replace(regex, '');
      cardData['link'] = blogPost.link;
      let pubDate = new Date(blogPost.date);
      cardData['date'] = `${pubDate.getMonthText()} ${pubDate.getDate()}, ${pubDate.getFullYear()}`;
      cardData['excerpt'] = blogPost.excerpt.rendered.replace(regex, '').substr(0, 180) + '...';
      cardData['cover'] = 'https://www.oorjan.com/blog/wp-content/uploads/2018/04/solar-power-e1522930664782.jpg'; // temp

      data.push(cardData);
    });

    return data;
  }

  /**
   * Fetches the posts from the WP API
   */
  getPosts() {
    const url = 'https://www.oorjan.com/blog/wp-json/wp/v2/posts';

    this.setState({
      ...this.state,
      loading: true,
    });

    let body = {
      ...this.state.search.filters
    };

    if( this.state.search.query ) {
      body['search'] = this.state.search.query;
    }

    fetchRequest(url, 'GET', body, false)
    .then((responseData) => {
      let list;
      list = this.parseSearchResponse(responseData);
      this.setState({
        list,
        loading: false,
        search: {
          ...this.state.search,
          lastQuery: this.state.search.query,
        }
      });
    });
  }

  componentWillMount() {
    // initial loading of the posts
    this.getPosts();
  }

	render() {
    let _handlers = {
      handleQueryChange: this.handleQueryChange,
      handleFilterChange: this.handleFilterChange,
      handleSearch: this.handleSearch,
      handleClear: this.handleClear,
    };

    let mainLoadingAnimCls = this.state.loading ? 'animate' : '';

		return (
			<div>
        <div class={"main-loader " + mainLoadingAnimCls}></div>
        <Header />
        <Search loading={this.state.loading} search={this.state.search} handlers={_handlers} />
        <List loading={this.state.loading} search={this.state.search} list={this.state.list} />
      </div>
		);
  }
}

export default App;
