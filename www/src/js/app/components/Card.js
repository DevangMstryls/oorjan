import React from "react";

class Card extends React.Component {
  constructor() {
    super();
  }

  render() {
    let info = this.props.info;

    return (
      <div class="card">
        <a href={info.link} target="_blank">
          <div class="card-inner">
            <div class="card-info">
              <div class="card-title">{info.title}</div>
              <div class="card-excerpt">{info.excerpt}</div>
              <div class="card-date">{info.date}</div>
            </div>
          </div>
        </a>
      </div>
    );
  }
}

export default Card;
