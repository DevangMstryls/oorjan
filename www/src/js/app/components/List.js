import React from "react";
import Card from "./Card";

class List extends React.Component {
  constructor() {
    super();
  }

  render() {
    let resultsHeaderTxt = "";
    if (this.props.loading) {
      resultsHeaderTxt = "Searching...";
    } else {
      if (this.props.search.lastQuery === "") {
        if (this.props.search.filters.order == "desc") {
          resultsHeaderTxt = "Latest Solar News";
        } else {
          resultsHeaderTxt = "Solar News (Oldest to Newest)";
        }
      } else {
        if (this.props.list.length) {
          resultsHeaderTxt = `Showing ${this.props.list.length} results for "${this.props.search.lastQuery}"`;
        } else {
          resultsHeaderTxt = 'The sun has set. No solar news coming in!';
        }
      }
    }

    return (
      <div class="list trans" id="list">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              {
                resultsHeaderTxt &&
                  <div class="results-header">{resultsHeaderTxt}</div>
              }
              <div class="results">
                {
                  this.props.list.map((blogPost, index) => {
                    return <Card key={index} info={blogPost} />;
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  componentDidUpdate() {
    var listEl = document.getElementById("list");
    if (this.props.list.length === 0) {
      listEl.classList.add("empty");
    } else {
      listEl.classList.remove("empty");
    }
  }
}

export default List;
