import React from "react";

class Header extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <nav>
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="logo">
                <img src="/static/assets/images/logo.png" alt="Oorjan" />
              </div>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;
