import React from "react";

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.toggleClearBtn = this.toggleClearBtn.bind(this);
  }

  /**
   * Handler for focus event
   */
  handleFocus() {
    this.toggleClearBtn();
    this.searchWrpr.classList.add('highlight-search');
  }

  /**
   * Handler for blur event
   */
  handleBlur() {
    if( this.props.search.query === '' ) {
      this.searchWrpr.classList.remove('highlight-search');
    } else {
      this.searchWrpr.classList.add('highlight-search');
    }
  }

  /**
   * Toggles the dislay of the clear button
   */
  toggleClearBtn() {
    let clearBtn = document.getElementById('clearBtn');
    this.props.search.query === '' ?
      clearBtn.classList.remove('show') : clearBtn.classList.add('show');
  }

  render() {
    let _handlers = this.props.handlers;
    let _filters = this.props.search.filters;

    return (
      <div class="search-section">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 txt-al-c">
              <h1 class="search-label">Search for some solar news</h1>

              <form method="get" onSubmit={_handlers.handleSearch}>
                <div class="search-wrpr" id="searchWrpr">
                  <input class="search-inp" type="search" name="search" id="search"
                    value={this.props.search.query} placeholder="e.g. Solar Inverters"
                    onChange={_handlers.handleQueryChange} autoComplete="off"
                    onFocus={this.handleFocus} onBlur={this.handleBlur} />
                  <div class="inp-btns">
                    <div class="btn inp-btn clear-btn trans" id="clearBtn"
                        onClick={_handlers.handleClear}>
                      <i class="icon-close"></i>
                    </div>
                    <button class="inp-btn search-btn trans" type="submit">
                      <i class="icon-search"></i>
                      <span class="search-btn-txt"> Search</span>
                    </button>
                  </div>
                </div>

                <div class="filters-wrpr">
                  <div class="filter">
                    <div class="radio-field">
                      <input type="radio" class="radio" id="orderbyDate" name="orderby"
                        value="date" checked={_filters.orderby === 'date'}
                        onChange={(e) => _handlers.handleFilterChange(e)} />
                      <label for="orderbyDate">
                        <span class="radio-btn"></span>
                        <span>Date</span>
                      </label>
                    </div>
                    <div class="radio-field">
                      <input type="radio" class="radio" id="orderbyRelevance" name="orderby"
                        value="relevance" checked={_filters.orderby === 'relevance'}
                        onChange={(e) => _handlers.handleFilterChange(e)} disabled={this.props.search.query === ''} />
                      <label class="trans" for="orderbyRelevance">
                        <span class="radio-btn"></span>
                        <span>Relevance</span>
                      </label>
                    </div>
                  </div>
                  <div class="filter">
                    <div class="radio-field">
                      <input type="radio" class="radio" id="orderDesc" name="order"
                        value="desc" checked={_filters.order === 'desc'}
                        onChange={(e) => _handlers.handleFilterChange(e)} />
                      <label for="orderDesc">
                        <span class="radio-btn"></span>
                        <span>Desc</span>
                      </label>
                    </div>
                    <div class="radio-field">
                      <input type="radio" class="radio" id="orderAsc" name="order"
                        value="asc" checked={_filters.order === 'asc'}
                        onChange={(e) => _handlers.handleFilterChange(e)} />
                      <label for="orderAsc">
                        <span class="radio-btn"></span>
                        <span>Asc</span>
                      </label>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.searchWrpr = document.getElementById('searchWrpr');
  }

  componentDidUpdate() {
    this.toggleClearBtn();
  }
}

export default Search;
