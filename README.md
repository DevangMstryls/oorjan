# README

## Setting up

- `cd` to the project directory
- Run: `npm install`

## Starting up

- Run: `gulp run-server`
- Go to: https://localhost:8000
